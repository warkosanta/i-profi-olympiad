﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    /// <summary>
    /// Note controller.
    /// </summary>
    [ApiController]
    [Route("notes")]
    public class Controller:ControllerBase
    {
        /*private readonly DataContext _context;
        private readonly int _firstNSimbolsOfText;
        public PrizeController(string n, DataContext context)
        {
            _context = context;
            _firstNSimbolsOfText = int.Parse(n);
        }

        /// <summary>
        /// Get all notes.
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public IEnumerable<Note> Get()
        {
            return _context.Notes;
        }

        /// <summary>
        /// Get note by id.
        /// </summary>
        /// <param name="id">Note identificator.</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        [Route("{id}")]
        public async Task<Note> Get(int id)
        {
            return await _context.Notes.FindAsync(id);
        }

        /// <summary>
        /// Get notes by query.
        /// </summary>
        /// <param name="query">Search query.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("query")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IEnumerable<Note> Get(string query)
        {
            if (string.IsNullOrEmpty(query))
            {
                foreach (var note in _context.Notes)
                {
                    note.Content = note.Content.Substring(0, _firstNSimbolsOfText);

                }
                return _context.Notes;
            }

            query = query.ToLower();
            return _context.Notes.Where(n => n.Title.ToLower().Contains(query)
            || n.Content.ToLower().Contains(query));

        }

        /// <summary>
        /// Post new note.
        /// </summary>
        /// <param name="note">New note.</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<StatusCodeResult> Post(NoteDto note)
        {
            await _context.Notes.AddAsync(new Note { Title = note.Title, Content = note.Content });
            await _context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Update note by id.
        /// </summary>
        /// <param name="id">Note identificator.</param>
        /// <param name="note">Data to update</param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut]
        [Route("{id}")]
        public async Task<StatusCodeResult> Put(int id, NoteDto note)
        {
            var oldNote = await _context.Notes.FindAsync(id);

            if (oldNote == null)
            {
                return BadRequest();
            }

            oldNote.Content = string.IsNullOrEmpty(note.Content) ? oldNote.Content : note.Content;
            oldNote.Title = string.IsNullOrEmpty(note.Title) ? oldNote.Title : note.Title;

            _context.Update(oldNote);
            await _context.SaveChangesAsync();
            return Ok();
        }

        /// <summary>
        /// Delete note by id.
        /// </summary>
        /// <param name="id">Note identificator.</param>
        /// <returns></returns>
        [HttpDelete]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<StatusCodeResult> Delete(int id)
        {
            var note = await _context.Notes.FindAsync(id);

            if (note == null)
            {
                return BadRequest();
            }

            _context.Notes.Remove(note);
            _context.SaveChanges();
            return Ok();
        }
        */
    }
}
