﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;


namespace API
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        public DbSet<Prize> Prizes { get; set; }
        public DbSet<Participant> Participants { get; set; }
        public DbSet<Promotion> Promotions { get; set; }
        public DbSet<Result> Results { get; set; }

    }
}
