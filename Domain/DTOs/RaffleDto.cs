﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DTOs
{
    public class RaffleDto
    {
        public Participant Winner { get; set; }
        public Prize Prize { get; set; }
    }
}
