﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.DTOs
{
    public  class PromoDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
